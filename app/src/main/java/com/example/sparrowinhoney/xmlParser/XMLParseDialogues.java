package com.example.sparrowinhoney.xmlParser;

import org.jetbrains.annotations.NotNull;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class XMLParseDialogues {
    private final List<Node> nodes;
    private final String[] avlbImages;

    public XMLParseDialogues(InputStream xml, String[] imgs) {
        nodes = new ArrayList<>();
        avlbImages = imgs;
        Arrays.sort(avlbImages);
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLHandler handler = new XMLHandler();
            parser.parse(xml, handler);
        } catch (ParserConfigurationException | SAXException e) {
            throw new IllegalArgumentException("Невозможно создать парсер! Ошибка:" + e.getLocalizedMessage());
        } catch (IOException e) {
            throw new IllegalArgumentException("Невозможно открыть xml-файл! Ошибка:" + e.getLocalizedMessage());
        }
    }

    public List<Node> getNodes() {
        return nodes;
    }

    /*public Boolean[][] getPassage() {
        List<Node> bufNds = new ArrayList<>(nodes.size());
        for (Node node : nodes)
            if (node.getCondition() != null)
                bufNds.add(node);

        Boolean[][] passage = new Boolean[bufNds.size()][bufNds.size()];
        int curRow;
        for (Node node : bufNds) {
            curRow = node.getId();

            for (Choice curCh : node.getCondition())
                passage[curRow][curCh.getNext()] = true;
        }
        return passage;
    }*/

    private class XMLHandler extends DefaultHandler {
        StringBuilder phrase;
        Node curNode;
        Choice curChoice;

        @Override
        public void startDocument() {
            phrase = new StringBuilder();
        }

        @Override
        public void startElement(String uri, String localName, String qName, @NotNull Attributes attributes) {
            String next = attributes.getValue("next");
            if ("node".equals(qName)) {
                // Иницализирует поля node
                String id = attributes.getValue("id");
                if (id == null)
                    throw new IllegalArgumentException("Блок node не имеет id!");
                String pers = attributes.getValue("pers");
                String back = attributes.getValue("rear");
                if (back != null && Arrays.binarySearch(avlbImages, back) < 0)     // Если картинки нет в массиве доступных
                    throw new IllegalArgumentException("Блок node с ID=" + id + " содержит некорректное значение атрибута rear!");
                // Создаём node и добавляем в список
                curNode = new Node(id, next, pers, back);
                nodes.add(curNode);
            } else if ("choice".equals(qName)) {
                // Иницализирует поля choice
                if (curNode.getPhrase() == null)
                    curNode.setPhrase(normalPhrase());
                if (curNode.getCondition() != null && curNode.getCondition().size() == 5)
                    throw new IllegalArgumentException("Невозможно добавить больше 5 условий!");
                curChoice = new Choice(next);
                curNode.addCondition(curChoice);
            } else if (!"resources".equals(qName))      // Если тек не node, не choice и не resources
                throw new IllegalArgumentException(qName + " - Некорректный тег");
            // Обнуляем строку
            phrase = new StringBuilder();
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            if ("node".equals(qName)) {
                if (curNode.getNext() == null && curNode.getCondition() == null)
                    throw new IllegalArgumentException("Блок node с ID=" + curNode.getId() + " не имеет атрибута next и не содержит условий!");
                if (curNode.getPhrase() == null)
                    curNode.setPhrase(normalPhrase());
                curNode = null;
            } else if ("choice".equals(qName)) {
                curChoice.setPhrase(normalPhrase());
                curChoice = null;
            }
        }

        @Override
        public void characters(@NotNull char[] ch, int start, int length) {
            // Ручной trim
            /*boolean flagStart=true, flagEnd=true;
            int end = start + length;
            for (int i = 0; i < length / 2; i++) {
                if (flagStart && (ch[start + i] == ' ' || ch[start + i] == '\n' || ch[start + i] == '\t'))
                    start++;
                else flagStart=false;
                if (flagEnd&&(ch[end - i] == ' ' || ch[end - i] == '\n' || ch[end - i] == '\t'))
                    end--;
                else flagEnd=false;
            }*/
            String addPhrase = new String(ch, start, length).trim();
            if (!addPhrase.equals(""))
                phrase.append(" ").append(addPhrase);
        }

        @NotNull
        private String normalPhrase() {
            if (phrase.length() == 0)    // Пробелов быть не может из за trim в characters()
                return "";
            return phrase.deleteCharAt(0).toString();
        }
    }
}