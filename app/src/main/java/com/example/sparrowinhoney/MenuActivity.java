package com.example.sparrowinhoney;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

public class MenuActivity extends AppCompatActivity {
    public static final String GAME_START = "gameStart";
    public static final int NEW_GAME = 1;
    public static final int CONTINUE_GAME = 2;
    public static SharedPreferences saves;
    static final String APP_SAVES = "saves";
    static final String APP_CUR_FRAME = "frame";
    public static final String APP_CUR_IMAGE = "image";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        saves = getSharedPreferences(APP_SAVES, Context.MODE_PRIVATE);
    }

    public void onStartClick(@NotNull View view) {
        int gameStart;
        Intent intent = new Intent(this, MainActivity.class);
        switch (view.getId()) {
            case R.id.btnNewGame:
                gameStart = NEW_GAME;
                break;
            case R.id.btnContinue:
                gameStart = CONTINUE_GAME;
                break;
            default:
                throw new IllegalArgumentException("Критическая ошибка!");
        }
        intent.putExtra(GAME_START, gameStart);
        startActivity(intent);
    }
}
