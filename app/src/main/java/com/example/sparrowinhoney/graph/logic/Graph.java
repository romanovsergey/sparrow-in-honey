package com.example.sparrowinhoney.graph.logic;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

public class Graph implements Comparable<Graph> {
    private String name;
    private int length;
    private boolean oriented;
    private Integer[][] data;

    public Graph() {
        this.name = "Graph";
        this.length = 0;
        this.oriented = false;
        this.data = new Integer[0][0];
    }

    public Graph(@NotNull String name, @NotNull Integer[][] data) {
        if (name.trim().equals(""))
            throw new IllegalArgumentException("Название графа не может быть пустым!");
        this.name = name;
        if (data.length == 0 || data.length != data[0].length)
            throw new IllegalArgumentException("Матрица смежности графа должна быть квадратной!");
        this.length = data.length;
        this.data = new Integer[data.length][data.length];
        this.data = cloneArray(data);
        isOriented();
    }

    public Graph(@NotNull String name, @NotNull Boolean[][] data) {
        if (name.trim().equals(""))
            throw new IllegalArgumentException("Название графа не может быть пустым!");
        this.name = name;
        if (data.length == 0 || data.length != data[0].length)
            throw new IllegalArgumentException("Матрица смежности графа должна быть квадратной!");
        this.length = data.length;
        this.data = new Integer[data.length][data.length];
        for (int i = 0; i < length; i++)
            for (int j = 0; j < length; j++)
                if (data[i][j])
                    this.data[i][j] = 0;
        isOriented();
    }

    public Graph(@NotNull Graph src) {
        this.name = src.name;
        this.length = src.length;
        this.oriented = src.oriented;
        this.data = new Integer[src.length][src.length];
        this.data = cloneArray(src.data);
    }

    @NotNull
    private static Integer[][] cloneArray(@NotNull Integer[][] src) {
        Integer[][] target = new Integer[src.length][src[0].length];
        for (int i = 0; i < src.length; i++) {
            System.arraycopy(src[i], 0, target[i], 0, src[i].length);
        }
        return target;
    }

    // Получить имя графа
    public String getName() {
        return name;
    }

    // Получить количество вершин
    public int getNumVer() {
        return length;
    }

    // Получить количество рёбер
    public int getNumEdg() {
        int sumEdg = 0;
        for (int i = 0; i < length; i++)
            for (int j = 0; j < length; j++)
                if (data[i][j] != null)
                    sumEdg++;
        return sumEdg;
    }

    // Получить вес ребра UV
    public Integer getWeightEdg(int u, int v) {
        checkCorrectVer(u);
        checkCorrectVer(v);
        return this.data[u][v];
    }

    // Задать вес ребра UV
    public void setWeightEdg(int u, int v, Integer value) {
        checkCorrectVer(u);
        checkCorrectVer(v);
        this.data[u][v] = value;
    }

    // Получить копию матрицы смежности
    public Integer[][] getMatrix() {
        return cloneArray(this.data);
    }

    // Сделавть граф ориентированным
    public void makeOriented() {
        oriented = true;
    }

    // Проверка, ориентированный ли граф?
    public boolean isOriented() {
        if (oriented) return true;
        else
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {
                    if (data[i][j] != data[j][i]) {
                        oriented = true;
                        return true;
                    }
                }
            }
        return false;
    }

    // Узнать степень заданной вершины (isOut=true: deg-, isOut=false: deg+)
    public int deg(int ver, boolean isOut) {
        checkCorrectVer(ver);
        int deg = 0;
        if (isOut)
            for (int i = 0; i < length; i++) {
                if (i != ver && this.data[ver][i] != null)
                    deg++;
            }
        else
            for (int i = 0; i < length; i++) {
                if (i != ver && this.data[i][ver] != null)
                    deg++;
            }
        return deg;
    }

    public int deg(int ver) {
        return deg(ver, true);
    }

    // Смежны ли две вершины?
    public boolean isCom(int ver1, int ver2) {
        checkCorrectVer(ver1);
        checkCorrectVer(ver2);
        return this.data[ver1][ver2] != null;
    }

    // Возвращает массив смежных вершин
    public int[] com(int ver) {
        checkCorrectVer(ver);
        int[] com = new int[deg(ver)];
        int c = 0;
        for (int i = 0; i < length; i++) {
            if (i != ver && this.data[ver][i] != null) {
                com[c] = i;
                c++;
            }
        }
        return com;
    }

    // Полностью удаляет вершину из графа
    public void dellVer(int ver) {
        remVer(ver);
        Integer[][] bufData = new Integer[length - 1][length - 1];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++)
                if (i != ver && j != ver)
                    bufData[i > ver ? i - 1 : i][j > ver ? j - 1 : j] = this.data[i][j];
        }
        this.data = bufData;
    }

    // Удаляет все рёбра, ведущие в/из вершины (в графе она остаётся)
    public void remVer(int ver) {
        checkCorrectVer(ver);
        for (int i = 0; i < length; i++) {
            this.data[i][ver] = null;
            this.data[ver][i] = null;
        }
    }

    // Удаляет ребро из графа
    public void remEdg(int ver1, int ver2) {
        checkCorrectVer(ver1);
        checkCorrectVer(ver2);
        if (oriented)
            this.data[ver1][ver2] = null;
        else {
            this.data[ver1][ver2] = null;
            this.data[ver2][ver1] = null;
        }
    }

    private void checkCorrectVer(int ver) {
        if (ver < 0 || ver > length - 1)
            throw new IllegalArgumentException("Некорректная вершина!");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Graph graph = (Graph) o;
        return length == graph.length &&
                oriented == graph.oriented &&
                Objects.equals(name, graph.name) &&
                Arrays.equals(data, graph.data);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, length, oriented);
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }

    @NotNull
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(name.length() + length * length * 2 + 16);
        sb.append(name).append(": ").append(length).append("\r\n");
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                sb.append(data[i][j] == null ? "-" : data[i][j]).append(" ");
            }
            sb.append("\r\n");
        }
        return sb.toString();
    }

    @Override
    public int compareTo(@NotNull Graph graph) {
        int dif = this.length - graph.length;
        if (dif == 0)
            dif = this.name.compareTo(graph.name);
        return dif;
    }
}