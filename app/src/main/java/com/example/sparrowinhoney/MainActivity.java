package com.example.sparrowinhoney;

import android.graphics.BitmapFactory;
import android.transition.TransitionManager;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintSet;

import com.example.sparrowinhoney.xmlParser.Node;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class MainActivity extends PreMainActivity {
    final long TEXT_TIMEOUT = 20;
    private boolean isPhraseDisplayed = false;
    private String curPhrase;
    private int idxPhrase;

    public void tvClick(View v) {
        // Если нажали кнопку когда выводится текст
        if (isPhraseDisplayed) {
            h.removeCallbacks(showText);
            tv.setText(curPhrase);
            h.removeCallbacks(postTextOutput);
            h.post(postTextOutput);
            return;
        }   // Или когда есть выборы
        else if (frame.getCondition() != null)
            return;
            // Или когда работают сенсоры
        else if (isTrackingSensor)
            return;

        // Получаем следующий узел
        frame = getNode(frame.getNext());
        if (frame == null)
            showErrorDialog("Невозможен переход на несуществующий node!");

        showFrame(frame);
    }

    @Override
    public void showFrame(@NotNull Node frm) {
        if(frm.getBackground()!=null)
            saves.edit().putString(MenuActivity.APP_CUR_IMAGE, frame.getId()).apply();
        setBackground(frm.getBackground());
        prsName.setText(frm.getPers());
        smoothlyPrint(frm.getPhrase());
    }

    private void smoothlyPrint(@NotNull String str) {
        curPhrase = str;
        idxPhrase = 0;
        isPhraseDisplayed = true;
        tv.setText("");
        for (int i = 0; i < curPhrase.length(); i++)
            h.postDelayed(showText, TEXT_TIMEOUT * i);
        h.postDelayed(postTextOutput, TEXT_TIMEOUT * curPhrase.length());
    }

    Runnable showText = () -> {
        tv.append(curPhrase.substring(idxPhrase, idxPhrase + 1));
        idxPhrase++;
    };

    Runnable postTextOutput = () -> {
        isPhraseDisplayed = false;
        if (frame.getId().equals("2"))
            startAccelerometer();

        if (frame.getCondition() != null) {
            ConstraintSet set = new ConstraintSet();
            set.clone(constraintLayout);
            // Отдельно привязываем верхнюю кнопку
            set.clear(btnsChoice[0].getId(), ConstraintSet.TOP);
            set.connect(btnsChoice[0].getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            btnsChoice[0].setText(frame.getCondition().get(0).getPhrase());
            // Затем все остальные к предыдущей
            for (int i = 1; i < Math.min(btnsChoice.length, frame.getCondition().size()); i++) {
                btnsChoice[i].setText(frame.getCondition().get(i).getPhrase());
                changeConstraints(set, i);
            }
            set.applyTo(constraintLayout);
            for (int i = 0; i < Math.min(btnsChoice.length, frame.getCondition().size()); i++)
                btnsChoice[i].setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(constraintLayout);
        }
    };

    private void changeConstraints(@NotNull ConstraintSet set, int i) {
        // Верх текущей к низу предыдущей
        set.clear(btnsChoice[i].getId(), ConstraintSet.TOP);
        set.connect(btnsChoice[i].getId(), ConstraintSet.TOP, btnsChoice[i - 1].getId(), ConstraintSet.BOTTOM);
        // Низ предыдущей к верху текущей
        set.clear(btnsChoice[i - 1].getId(), ConstraintSet.BOTTOM);
        set.connect(btnsChoice[i - 1].getId(), ConstraintSet.BOTTOM, btnsChoice[i].getId(), ConstraintSet.TOP);
    }

    public void btnChoiceClick(View v) {
        ConstraintSet set = new ConstraintSet();
        set.clone(constraintLayout);
        for (int i = 0; i < Math.min(btnsChoice.length, frame.getCondition().size()); i++) {
            btnsChoice[i].setText("");
            // По-умолчанию
            set.clear(btnsChoice[i].getId(), ConstraintSet.TOP);
            set.clear(btnsChoice[i].getId(), ConstraintSet.BOTTOM);
            set.connect(btnsChoice[i].getId(), ConstraintSet.BOTTOM, R.id.prsName, ConstraintSet.TOP);
        }
        set.applyTo(constraintLayout);
        for (int i = 0; i < Math.min(btnsChoice.length, frame.getCondition().size()); i++)
            btnsChoice[i].setVisibility(View.INVISIBLE);
        TransitionManager.beginDelayedTransition(constraintLayout);
        switch (v.getId()) {
            case R.id.btnChoice1:
                frame = getNode(frame.getCondition().get(0).getNext());
                break;
            case R.id.btnChoice2:
                frame = getNode(frame.getCondition().get(1).getNext());
                break;
            case R.id.btnChoice3:
                frame = getNode(frame.getCondition().get(2).getNext());
                break;
            case R.id.btnChoice4:
                frame = getNode(frame.getCondition().get(3).getNext());
                break;
            case R.id.btnChoice5:
                frame = getNode(frame.getCondition().get(4).getNext());
                break;
        }
        showFrame(frame);
    }

    @Override
    protected void tiltAction() {
        tvClick(tv);
    }

    @Override
    protected void setBackground(String image) {
        if (image != null) {
            try {
                background.setImageBitmap(BitmapFactory.decodeStream(getAssets().open("images/" + image)));
            } catch (IOException e) {
                showErrorDialog(e.getLocalizedMessage());
            }
        }
    }
}