package com.example.sparrowinhoney.graph.logic;

import android.os.Build;

import androidx.annotation.RequiresApi;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import java.io.*;
import java.util.*;
import java.util.function.Predicate;

public class SetFileGraph implements Collection<Graph> {
    private Set<Graph> GFS;
    private static File file;
    public static final String[] idOfNew = {"new", "новый", "создать"};

    public SetFileGraph(File f) {
        file = f;
        GFS = new TreeSet<>();
        updateGFS();
    }

    public SetFileGraph(File f, @NotNull Graph... Gs) {
        this(f);
        GFS.addAll(Arrays.asList(Gs));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void updateGFS() {
        try (FileReader reader = new FileReader(file)) {
            Scanner fscan = new Scanner(reader);
            while (fscan.hasNextLine()) {
                String[] strSplit = fscan.nextLine().split(":");
                String name = strSplit[0].trim();
                if (strSplit.length == 2 && isNormName(name) && getIf(graph -> graph.getName().toLowerCase().equals(name.toLowerCase())) == null) {
                    int n = Integer.parseInt(strSplit[1].trim());
                    Integer[][] data = new Integer[n][n];
                    int i, j = 0;
                    boolean flag = false;
                    for (i = 0; i < n && !flag; i++) {
                        for (j = 0; j < n; j++)
                            if (fscan.hasNextInt())
                                data[i][j] = fscan.nextInt();
                            else if (fscan.next().toLowerCase().equals("-")) {
                                data[i][j] = null;
                            } else {
                                flag = true;
                                break;
                            }
                    }
                    if (flag && (i != n - 1 || j != n - 1))
                        throw new IllegalArgumentException("Некорректно указаны размеры графа!");
                    GFS.add(new Graph(name, data));
                }
            }
        } catch (FileNotFoundException ex) {
            throw new IllegalArgumentException("Файл \"" + file.toString() + "\" не найден!");
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public void writeAllToFile() {
        try (FileWriter writer = new FileWriter(file)) {
            for (Graph bufG : GFS) {
                writer.write(bufG.toString());
            }
            writer.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException("Нет доступа к файлу!");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Graph get(String name) {
        Graph out = getOnName(name);
        if (out == null)
            throw new IllegalArgumentException("Граф с таким именем не найден!");
        return out;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private Graph getOnName(String name){
        return getIf(graph -> graph.getName().toLowerCase().equals(name.toLowerCase()));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Graph getIf(Predicate<Graph> lambda) {
        for (Graph G : GFS) {
            if (lambda.test(G))
                return G;
        }
        return null;
    }

    @Contract("null -> false")
    private boolean isNormName(String name) {
        return name != null && name.length() != 0 && !name.contains(" ") && !isNewName(name);
    }

    public static boolean isNewName(@NotNull String name) {
        name = name.trim().toLowerCase();
        for (String str : idOfNew) {
            if (name.equals(str))
                return true;
        }
        return false;
    }

    public String getListNames(String splitter) {
        StringBuilder sb = new StringBuilder();
        for (Graph G : GFS) {
            sb.append(G.getName()).append(splitter);
        }
        sb.replace(sb.length() - splitter.length(), sb.length(), "");
        return sb.toString();
    }

    @Override
    public boolean remove(@NotNull Object o) {
        if (o instanceof Graph) {
            return GFS.remove(o);
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean remove(@NotNull String name) {
        return GFS.removeIf(graph -> graph.getName().toLowerCase().equals(name.toLowerCase()));
    }

    @Override
    public boolean add(@NotNull Graph graph) {
        return GFS.add(graph);
    }

    @Override
    public int size() {
        return GFS.size();
    }

    @Override
    @Contract(pure = true)
    public boolean isEmpty() {
        return GFS.isEmpty();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void clear() {
        GFS.clear();
        updateGFS();
    }

    @Override
    @Contract(pure = true)
    public boolean contains(Object o) {
        return GFS.contains(o);
    }

    @NotNull
    @Override
    public Iterator<Graph> iterator() {
        return GFS.iterator();
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return GFS.toArray();
    }

    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] ts) {
        return GFS.toArray(ts);
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends Graph> collection) {
        return false;
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> collection) {
        return false;
    }

    @Override
    @Contract(value = "null -> false", pure = true)
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SetFileGraph graphs = (SetFileGraph) o;
        return Objects.equals(GFS, graphs.GFS);
    }

    @Override
    public int hashCode() {
        return Objects.hash(GFS);
    }

    @Override
    public String toString() {
        return "SetFileGraph{" +
                "GFS=" + GFS +
                '}';
    }
}