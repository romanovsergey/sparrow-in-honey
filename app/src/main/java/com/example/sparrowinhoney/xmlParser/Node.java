package com.example.sparrowinhoney.xmlParser;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private final String id;
    private final String next;
    private final String pers;
    private final String background;
    private String phrase;
    private List<Choice> condition;

    Node(String id, String next, String pers, String background) {
        this.id = id;
        this.next = next;
        this.pers = pers;
        this.background = background;
        this.phrase = null;
    }

    public String getId() {
        return id;
    }

    public String getNext() {
        return next;
    }

    public String getPers() {
        return pers;
    }

    public String getBackground() {
        return background;
    }

    public String getPhrase() {
        return phrase;
    }

    public List<Choice> getCondition() {
        return condition;
    }

    void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    void addCondition(Choice ch) {
        if (condition == null)
            condition = new ArrayList<>(5);
        condition.add(ch);
    }
}