package com.example.sparrowinhoney.xmlParser;

public class Choice {
    private final String next;
    private String phrase;

    Choice(String next) {
        if (next == null)
            throw new IllegalArgumentException("В choice обязательный атрибут next!");
        this.next = next;
        this.phrase = null;
    }

    public String getPhrase() {
        return phrase;
    }

    public String getNext() {
        return next;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }
}