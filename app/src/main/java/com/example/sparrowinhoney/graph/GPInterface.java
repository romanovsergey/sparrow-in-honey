package com.example.sparrowinhoney.graph;

import com.example.sparrowinhoney.graph.logic.*;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import java.util.Arrays;
import java.util.Scanner;

class GPInterface {
    // Класс включает в себя пользовательский интерфейс
    // Каждый метод выводит, либо запрашивает ввод с консоли
    // Никакие исключения не кидаются, все ошибки выводятся на консоль
    // Функции, помеченные префиксом "_" возвращают введённое значение

    private static Scanner input = new Scanner(System.in);
    public static final int maxTypeIn = 3;

    private static int inputInt(int inf, int sup) {
        int buf = input.hasNextInt() ? input.nextInt() : inf - 1;
        while (buf < inf || buf > sup) {
            System.out.println("Неверный ввод, повторите попытку");
            buf = input.hasNextInt() ? input.nextInt() : inf - 1;
        }
        return buf;
    }

    private static String inputName() {
        String name = input.hasNext() ? input.next() : SetFileGraph.idOfNew[0];
        while (SetFileGraph.isNewName(name)) {
            System.out.println("Неверный ввод, повторите попытку");
            name = input.hasNext() ? input.next() : SetFileGraph.idOfNew[0];
        }
        return name;
    }

    @NotNull
    static Graph newGraph() {
        /*Тип*/
        System.out.println("Выберите тип графа:\n1 - Взвешанный\n2 - Не взвешанный");
        int grade = inputInt(1, 2);
        /*Метод*/
        System.out.println("Выберите метод ввода графа:\n1 - Поэтапно (для орграфа)\n2 - Поэтапно (для не орграфа)\n3 - Матрица смежности\nМатрица инцинденции и Список рёбер ещё в разработке");
        int typeIn = inputInt(1, maxTypeIn);
        /*Имя*/
        System.out.print("Введите имя графа:  ");
        String name = inputName();
        /*Вершины*/
        System.out.print("Введите количество вершин:  ");
        int n = inputInt(1, Integer.MAX_VALUE);
        return newGraph(name, n, grade == 1, typeIn);
    }

    @NotNull
    @Contract("_, _, _, _ -> new")
    static Graph newGraph(String name, int numVer, boolean weight, int typeIn) {
        if (typeIn < 1 || typeIn > maxTypeIn || numVer < 1 || SetFileGraph.isNewName(name))
            throw new IllegalArgumentException("Некорректные параметры!");
        Integer[][] G = new Integer[numVer][numVer];
        // Заполнение
        switch (typeIn) {
            case 1:
                if (weight)
                    entryOrgraphW(G);
                else
                    entryOrgraph(G);
                break;
            case 2:
                if (weight)
                    entryNoOrgraphW(G);
                else
                    entryNoOrgraph(G);
                break;
            case 3:
                entryAdjacencyMatrix(G);
                break;
            default:
                throw new IllegalArgumentException("Неопределённый метод ввода графа!");
        }
        return new Graph(name, G);
    }

    private static void printOldVer(@NotNull Integer[][] G, int i) {
        boolean flag = false;
        System.out.print((i + 1) + ":  ");
        for (int j = 0; j < G.length; j++) {
            if (G[i][j] != null) {
                System.out.print((j + 1) + " ");
                flag = true;
            }
        }
        if (flag)
            System.out.print("и ");
    }

    static void entryNoOrgraph(@NotNull Integer[][] G) {
        System.out.println("Формат ввода:\nТекущая_вершина:  Список_Исходящих_Из_Неё...\nДля окончания ввода - 0");
        for (int i = 0; i < G.length; i++) {
            // Ищем и выводим уже существующие смежные
            printOldVer(G, i);
            // Вводим новые смежные
            while (true) {
                int buf = inputInt(0, G.length);
                if (buf != 0) {
                    G[i][buf - 1] = 0;
                    G[buf - 1][i] = 0;
                } else break;
            }
        }
    }

    static void entryNoOrgraphW(@NotNull Integer[][] G) {
        System.out.println("Формат ввода:\nТекущая_вершина:  Список_Исходящих_Из_Неё_и_Их_Веса...\nПосле каждой вершины пишите вес получившейся дуги через пробел\nДля окончания ввода - 0");
        for (int i = 0; i < G.length; i++) {
            printOldVer(G, i);
            while (true) {
                int buf = inputInt(0, G.length);
                if (buf != 0) {
                    int w = inputInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
                    G[i][buf - 1] = w;
                    G[buf - 1][i] = w;
                } else break;
            }
        }
    }

    static void entryOrgraph(@NotNull Integer[][] G) {
        System.out.println("Формат ввода:\nТекущая_вершина:  Список_Исходящих_Из_Неё...\nДля окончания ввода - 0");
        for (int i = 0; i < G.length; i++) {
            // Вводим новые смежные
            while (true) {
                int buf = inputInt(0, G.length);
                if (buf != 0)
                    G[i][buf - 1] = 0;
                else break;
            }
        }
    }

    static void entryOrgraphW(@NotNull Integer[][] G) {
        System.out.println("Формат ввода:\nТекущая_вершина:  Список_Исходящих_Из_Неё_и_Их_Веса...\nПосле каждой вершины пишите вес получившейся дуги через пробел\nДля окончания ввода - 0");
        for (int i = 0; i < G.length; i++) {
            while (true) {
                int buf = inputInt(0, G.length);
                if (buf != 0)
                    G[i][buf - 1] = inputInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
                else break;
            }
        }
    }

    static void entryAdjacencyMatrix(@NotNull Integer[][] G) {
        System.out.println("Введите саму матрицу смежности.\nДля обозначения несмежных вершин введите не число (обычно '-')");
        for (int i = 0; i < G.length; i++) {
            for (int j = 0; j < G.length; j++) {
                G[i][j] = input.hasNextInt() ? input.nextInt() : null;
            }
        }
    }

    // Прочие методы, используемые в Main'e

    static void inputParam(String[] param) {
        input = new Scanner(System.in);
        Arrays.fill(param, "");
        String[] strArr = input.nextLine().split(" ");
        for (String word : strArr) {
            if (!word.equals("")) {
                for (int i = 0; i < param.length; i++)
                    if (param[i].equals("")) {
                        param[i] = word.toLowerCase();
                        break;
                    }
            }
        }
    }

    static boolean _chain() {
        System.out.println("Попробовать найти Гамильтонову цепь?\t0 - нет, 1 - да");
        int buf = input.nextInt();
        while (buf != 0 && buf != 1) {
            System.out.println("Неверный ввод, повторите попытку");
            input.nextLine();
            buf = input.nextInt();
        }
        return buf == 1;
    }

    static int _multistart(int n) {
        System.out.println("Использовать мультистарт или ввести начальную веришну?");
        System.out.println("0 - мультистарт, 1-" + n + " - выбрать вершину с указанным номером для старта");
        int buf = input.nextInt();
        while (buf < 0 || buf > n) {
            System.out.println("Неверный ввод, повторите попытку");
            input.nextLine();
            buf = input.nextInt();
        }
        return buf;
    }

    static boolean _tree() {
        System.out.println("Нарисовать дерево поиска?\t0 - нет, 1 - да");
        int buf = input.nextInt();
        while (buf != 0 && buf != 1) {
            System.out.println("Неверный ввод, повторите попытку");
            input.nextLine();
            buf = input.nextInt();
        }
        return buf == 1;
    }
}