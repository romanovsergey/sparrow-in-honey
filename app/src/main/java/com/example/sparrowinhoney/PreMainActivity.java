package com.example.sparrowinhoney;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.sparrowinhoney.xmlParser.Node;
import com.example.sparrowinhoney.xmlParser.XMLParseDialogues;

import org.jetbrains.annotations.NotNull;

import java.util.List;

class PreMainActivity extends AppCompatActivity implements SensorEventListener {
    ConstraintLayout constraintLayout;
    SharedPreferences saves;

    // Текст
    TextView tv;
    TextView prsName;
    TextView mZValueText;
    Button[] btnsChoice;

    // Датчики
    SensorManager sensorManager;
    Sensor accelerometer;
    boolean isTrackingSensor;
    float firstTrack;

    Node frame;           // Текущий слайд игры
    ImageView background;   // Задний фон
    List<Node> nodes;
    Handler h;          // Для плавного вывода текста

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        );*/

        setContentView(R.layout.activity_main);

        // Привязываем объекты к нашей разметке
        constraintLayout = findViewById(R.id.activity_main);
        tv = findViewById(R.id.textView);
        prsName = findViewById(R.id.prsName);
        mZValueText = findViewById(R.id.value_z);
        background = findViewById(R.id.background);
        btnsChoice = new Button[5];
        btnsChoice[0] = findViewById(R.id.btnChoice1);
        btnsChoice[1] = findViewById(R.id.btnChoice2);
        btnsChoice[2] = findViewById(R.id.btnChoice3);
        btnsChoice[3] = findViewById(R.id.btnChoice4);
        btnsChoice[4] = findViewById(R.id.btnChoice5);

        /*// Посимвольный вывод текства в textView
        on = new Handler(msg -> {
            String str = String.valueOf(Character.toChars(msg.what));
            tv.append(str);
            return true;
        });*/
        // Действие после вывода текста
        h = new Handler();

        // Менеджер сенсоров для работы с акселерометром
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null)
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        try {
            // Считываем XML-файл
            XMLParseDialogues xParse = new XMLParseDialogues(getAssets().open("dialogues.xml"), getAssets().list("images"));
            nodes = xParse.getNodes();
            //nodes.sort(Comparator.comparing(Node::getId));
            int gameStart = getIntent().getIntExtra(MenuActivity.GAME_START, 0);
            saves = MenuActivity.saves;
            if (saves == null)
                throw new IllegalArgumentException("Критическая ошибка!");
            if (gameStart == 1)
                frame = getNode("pre");
            else if (gameStart == 2)
                frame = getNode(saves.getString(MenuActivity.APP_CUR_FRAME, "pre"));
            else throw new IllegalArgumentException("Критическая ошибка!");
            showFrame(frame);
            String curImg = saves.getString(MenuActivity.APP_CUR_IMAGE, frame.getId());
            if (!frame.getId().equals(curImg))
                setBackground(getNode(curImg).getBackground());
        } catch (Exception e) {
            showErrorDialog(e.getLocalizedMessage());
        }
    }

    @SuppressLint("CommitPrefEdits")
    protected void onPause() {
        super.onPause();
        if (isTrackingSensor)
            sensorManager.unregisterListener(this);
        // Сохранение игры
        saves.edit().putString(MenuActivity.APP_CUR_FRAME, frame.getId()).apply();
    }

    protected void onResume() {
        super.onResume();
        if (isTrackingSensor)
            startAccelerometer();
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        );
    }

    public void showFrame(@NotNull Node frm) {
    }

    protected void setBackground(String image) {
    }

    protected void showErrorDialog(String error) {
        final AlertDialog aboutDialog = new AlertDialog.Builder(this)
                .setMessage(error)
                .setPositiveButton("OK", (dialog, which) -> this.finish()).create();
        aboutDialog.show();
    }

    protected Node getNode(String id) {
        for (Node nd : nodes) {
            if (nd.getId().equals(id))
                return nd;
        }
        return null;
    }

    public void startAccelerometer() {
        isTrackingSensor = true;
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(@NotNull SensorEvent event) {
        // Распознавание наклона телефона на 3 пункта
        if (isTrackingSensor && event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (firstTrack == 0.0)
                firstTrack = event.values[2];
            else if (Math.abs(firstTrack - event.values[2]) > 3) {
                sensorManager.unregisterListener(this);
                isTrackingSensor = false;
                tiltAction();
            }
            mZValueText.setText(String.valueOf(event.values[2]));
        }
    }

    protected void tiltAction() {
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}