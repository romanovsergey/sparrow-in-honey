package com.example.sparrowinhoney.graph;

import com.example.sparrowinhoney.graph.logic.Graph;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import java.util.*;

class GPLogic {
    // Класс содержит алгоритмы работы с графами
    // Не выводит и не ожидает ввода из консоли, не работает с файлами, не кидает Checked-исключения
    // Некоторые функции кидают IllegalArgumentException

    @NotNull
    @Contract(pure = true)
    private static int[] com(@NotNull Graph G, int ver, final boolean[] pass) {
        List<Integer> outVer = new ArrayList<>(G.getNumVer());
        for (int i = 0; i < G.getNumVer(); i++) {
            if (i != ver && G.isCom(ver, i) && !pass[i]) {
                outVer.add(i);
            }
        }
        int[] com = new int[outVer.size()];
        for (int i = 0; i < com.length; i++)
            com[i] = outVer.get(i);
        return com;
    }

    @Contract(pure = true)
    private static boolean isFull(@NotNull final boolean[] arr) {
        for (boolean b : arr) {
            if (!b)
                return false;
        }
        return true;
    }

    @NotNull
    static String EulerCycle(final Graph G, int start) {
        Graph Gcopy = new Graph(G);
        Stack<Integer> ST = new Stack<>();
        Stack<Integer> EC = new Stack<>();
        ST.push(start);
        int curV = start - 1;
        int nxtV = 0;
        while (!ST.empty()) {
            if (Gcopy.isCom(curV, nxtV)) {       // Если существует ребро из curV+1 вершины в nxtV+1 вершину
                ST.push(nxtV + 1);
                Gcopy.remEdg(curV, nxtV);        // Удаляем эти рёбра
                curV = nxtV;                     // Переходим к следущей вершине
                nxtV = 0;
            } else if (nxtV == G.getNumVer() - 1) {   // Если перебрали все и ничего не подошло
                EC.push(ST.pop());               // Перекладываем вершину в другой стек
                if (!ST.empty())
                    curV = ST.peek() - 1;
                nxtV = 0;
            } else nxtV++;
        }
        StringBuilder out = new StringBuilder();
        while (!EC.empty()) {
            out.append(EC.pop()).append(" ");
        }
        return out.toString();
    }

    @Contract(pure = true)
    static boolean IsHamCycle(@NotNull final Graph G) {
        // По теореме Дираха
        for (int i = 0; i < G.getNumVer(); i++) {
            if (G.deg(i) < (double) G.getNumVer() / 2)
                return false;
        }
        return true;
    }

    @NotNull
    static String HamiltonCycle(final Graph G, int start, boolean cycle, boolean tree) {
        if (tree)
            System.out.println("Дерево поиска:\n►" + start);
        start--;
        int curV = start;
        boolean[] pass = new boolean[G.getNumVer()];        // Массив пройденных вершин
        pass[start] = true;
        Stack<Integer> HC = new Stack<>();                  // Список пройденных вершин
        HC.push(curV);
        int[] com;                                          // Массив смежных вершин
        int block = G.getNumVer() - 1;                      // Счётчик для разблокировки 1-й вершины
        boolean back = false;                               // Если мы вернулись назад
        boolean[] treeArr = new boolean[G.getNumVer()];     // Вспомогательный массив для рисования дерева

        while (!isFull(pass)) {
            if (HC.isEmpty()) {
                throw new IllegalArgumentException("Граф не содержит Гамильтонов" + (cycle ? "а цикла!" : "ой цепиот заданной вершины!"));
            }
            // Получаем массив допустимых для движения вершин
            com = com(G, HC.peek(), pass);
            if (com.length > 0) {
                if (back)  // Если вернулись назад на ветвление
                {
                    if (com.length > 1 && curV < com[com.length - 1]) {     // Непройденное ветвление
                        // Переходим на следующую ветку, относительно той, с которой пришли
                        curV = com[Arrays.binarySearch(com, curV) + 1];
                        back = false;
                        pass[curV] = true;
                        HC.push(curV);
                        block--;
                    } else {                                                // Пройденное ветвление
                        // Откатываемся до ближайшего ветвления
                        do {
                            curV = HC.pop();
                            pass[curV] = false;
                            pass[start] = true;
                            block++;
                        } while (!HC.isEmpty() && com(G, HC.peek(), pass).length < 2);
                        back = true;
                    }
                } else {
                    // Если идём вперёд
                    curV = com[0];
                    pass[curV] = true;
                    HC.push(curV);
                    block--;
                }
            } else {    // Если тупик
                // Откатываемся до ближайшего ветвления
                do {
                    curV = HC.pop();
                    pass[curV] = false;
                    pass[start] = true;
                    block++;
                } while (!HC.isEmpty() && com(G, HC.peek(), pass).length < 2);
                back = true;
            }
            if (block == 0 && cycle)
                pass[start] = false;
            // Построение дерева поиска
            if (tree && !back) {
                int i = 0;
                for (; i < HC.size() - 2; i++) {
                    // Вниз от └► (false) всегда печатаем " ", а снизу от ├► - " │"
                    if (!treeArr[i])
                        System.out.print("  ");
                    else
                        System.out.print(" │");
                }
                if (Arrays.binarySearch(com, curV) == com.length - 1) {   // Если вершина последняя в ветке
                    System.out.println(" └►" + (curV + 1));
                    treeArr[i] = false;
                } else {
                    System.out.println(" ├►" + (curV + 1));
                    treeArr[i] = true;
                }
            }
        }
        StringBuilder out = new StringBuilder();
        out.append("ГЦ:  ");
        for (int v : HC) {
            out.append(v + 1).append(" ");
        }
        return out.toString();
    }

    static boolean isRoute(@NotNull final Graph G, int start, int end, @NotNull Stack<Integer> path) {
        // Используем метод обхода в ширину
        path.clear();
        Deque<Integer> q = new ArrayDeque<>();          // Очередь посещённых вершин
        q.offer(start);
        boolean[] used = new boolean[G.getNumVer()];
        used[start] = true;
        int[] parents = new int[G.getNumVer()];         // Массив родителя каждой вершины (т.е. из какой вершины мы попали в данную)
        parents[start] = -1;
        int curV;
        int[] com;
        while (!q.isEmpty()) {
            curV = q.poll();
            if (curV == end) {                          // Если дошли до конечной вершины
                for (int v = end; v != -1; v = parents[v])
                    path.push(v);
                return true;
            }
            com = G.com(curV);
            for (int nxtV : com) {
                if (!used[nxtV]) {                      // Если вершина не посещённая
                    used[nxtV] = true;
                    q.offer(nxtV);
                    parents[nxtV] = curV;
                }
            }
        }
        return false;
    }

    @NotNull
    @Contract("_ -> new")
    static int[] connectivity(@NotNull final Graph G) {
        // Находим k непересекающихся цепей
        int vk = 0, rk = 0;
        int minVk = G.getNumVer(), minRk = G.getNumVer();
        Graph Gcpy;
        Stack<Integer> path = new Stack<>();
        for (int i = 0; i < G.getNumVer(); i++) {
            for (int j = i + 1; j < G.getNumVer(); j++) {
                Gcpy = new Graph(G);                                   // Вершинная связность
                while (isRoute(Gcpy, i, j, path) && vk < minVk) {
                    vk++;
                    if (path.size() == 2)   // если цепь - всего одно ребро
                        Gcpy.remEdg(path.pop(), path.peek());
                    else {
                        path.pop();
                        while (path.size() > 1) {
                            Gcpy.remVer(path.pop());
                        }
                    }
                }
                Gcpy = new Graph(G);                                  // Рёберная связность
                while (isRoute(Gcpy, i, j, path) && rk < minRk) {
                    rk++;
                    while (path.size() > 1) {
                        Gcpy.remEdg(path.pop(), path.peek());
                    }
                }
                if (minVk == 0 && minRk == 0)
                    return new int[]{0, 0};
                minVk = vk;
                minRk = rk;
                vk = 0;
                rk = 0;
            }
        }
        return new int[]{minVk, minRk};
    }

    static boolean planarity(final Graph G) {
        System.out.println("Эта функция ещё в разработке...");
        return false;
    }

    private static void changeRowCol(@NotNull Integer[][] G, int curRC, int nxtRC) {
        Integer[] buf;
        buf = G[curRC];
        G[curRC] = G[nxtRC];
        G[nxtRC] = buf;
    }

    public static boolean isomorphism(@NotNull final Graph G1, @NotNull final Graph G2) {
        // Если разное количество вершин, то точно не изоморфны
        if (G1.getNumVer() != G2.getNumVer())
            return false;
        // Если разное количество рёбер, то не изоморфны
        int sumEdg = 0;
        for (int i = 0; i < G1.getNumVer(); i++) {
            for (int j = 0; j < G1.getNumVer(); j++) {
                if (G1.isCom(i, j))
                    sumEdg++;
                if (G2.isCom(i, j))
                    sumEdg--;
            }
        }
        if (sumEdg != 0)
            return false;
        // Если разные степенные последовательности, то тоже не изоморфны
        int[] degSeq1 = new int[G1.getNumVer()];
        int[] degSeq2 = new int[G1.getNumVer()];
        for (int i = 0; i < G1.getNumVer(); i++) {
            degSeq1[i] = G1.deg(i);
            degSeq2[i] = G2.deg(i);
        }
        Arrays.sort(degSeq1);
        Arrays.sort(degSeq2);
        if (!Arrays.equals(degSeq1, degSeq2))
            return false;

        // Если всё прерыдущее не помогло, то - полный перебор
        Integer[][] mtx1 = G1.getMatrix();
        Integer[][] mtx2 = G2.getMatrix();
        for (int i = 0; i < mtx1.length; i++)
            for (int j = i + 1; j < mtx1.length; j++) {
                changeRowCol(mtx2, i, j);
                if (Arrays.deepEquals(mtx1, mtx2))
                    return true;
            }
        return false;
    }

    private static boolean isK5(@NotNull Graph G) {
        Integer[][] k5 = {{0, 1, 1, 1, 1},
                {1, 0, 1, 1, 1},
                {1, 1, 0, 1, 1},
                {1, 1, 1, 0, 1},
                {1, 1, 1, 1, 0}};
        return Arrays.deepEquals(G.getMatrix(), k5);
    }

    private static boolean isK33(@NotNull Graph G) {
        Integer[][] k33 = {{0, 1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0}};
        return Arrays.deepEquals(G.getMatrix(), k33);
    }

    /*public static boolean isPlanar(final Graph G) {

    }

    private void DFSPlanar(@NotNull final Graph G, List<Integer> cycle, boolean[] pass, int curVer) {
        // Идём по прямой, если нет развилок
        while (G.deg(curVer) < 2) {
            int[] com = G.com(curVer);
            if (com.length == 1)
                curVer = com[0];
            else return;
            cycle.add(curVer);
        }
        for()
    }*/

    public static boolean isSymmetric(@NotNull final Graph G) {
        Integer[][] data = G.getMatrix();
        for (int i = 0; i < G.getNumVer(); i++)
            for (int j = 0; j < G.getNumVer(); j++)
                if (!data[i][j].equals(data[j][i]))
                    return false;
        return true;
    }

    public static boolean isTree(@NotNull final Graph G) {
        return connectivity(G)[0] >= 1 && G.getNumEdg() == G.getNumVer() - 1;
    }


    @FunctionalInterface
    public interface TernaryOperator<T> {
        T apply(T u, T v, T w);

        String toString();

        boolean equals(Object o);
    }

    public static void redo(@NotNull final Graph G, TernaryOperator<Integer> lambda) {
        for (int i = 0; i < G.getNumVer(); i++)
            for (int j = 0; j < G.getNumVer(); j++)
                if (G.getWeightEdg(i, j) != null) {
                    G.setWeightEdg(i, j, lambda.apply(i + 1, j + 1, G.getWeightEdg(i, j)));
                }
    }
}